import cv2
import pyautogui

flag2 = True
video = cv2.VideoCapture(0)
face_cascade = cv2.CascadeClassifier("facefeature.xml")
while True:
    check, frame = video.read()
    gary = frame
    faces = face_cascade.detectMultiScale(gary, scaleFactor=1.05, minNeighbors=5)
    flag = True

    for x, y, w, h in faces:
        font = cv2.FONT_HERSHEY_COMPLEX
        cv2.putText(gary, 'Present', (20, 70), font, 2, (255, 0, 0), 5, cv2.LINE_AA)
        flag = False
        gary = cv2.rectangle(gary, (x, y), (x+w, y+h), (0, 255, 0), 2)
        if not flag2:
            pyautogui.hotkey('shift', '!')
            flag2 = True

    if flag:
        font = cv2.FONT_HERSHEY_COMPLEX
        cv2.putText(gary, 'Not Present', (20, 70), font, 2, (255, 0, 0), 5, cv2.LINE_AA)
        if flag2:
            pyautogui.hotkey('shift', '@')
            flag2 = False
    cv2.imshow('face', gary)
    key = cv2.waitKey(1)
    if key == ord('q'):
        break

video.release()
cv2.destroyAllWindows()
