# MediaControls
Video Pauses and Plays using Face Detection<br/>
Hot Keys :<br/>
Set Play only - 'Shift' + '1'<br/>
Set Pause only - 'Shift' + '2'

# gesture_detect.py
Detects the hand gestures and does the following
1 => volume up
2 => volume down
3 => mute
4 => next track
5 => quit and exit

set the hot keys in vlc accordingly
